var { assert } = require("chai");

var { User, Todo } = require("../../services");

var db = require("../../db");

describe('Todo', function() {
    let userid;
    const props = ['id','userid','title','completed'];
    let todoid;
    it('Debe guardar un Todo', async () => {
        try {
            var client = await db.connect();
            //Agrega información previa
            var query = User.insert({
                name:"ruben",
                username:"rubentest",
                email:"ruben@ruben.cl",
                phone:"9123345",
                website:"ruben.cl",
            });
            var { rows } = await client.query(query.toParam());
            userid = rows[0].id;

            var query = Todo.insert({
                userid:userid,
                title:"Titulo test",
                completed:true
            });
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
            todoid = rows[0].id;
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener el Todo por ID', async() => {
        try {
            var client = await db.connect();
            var query = Todo.getByPk(todoid);
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener todos los Todos', async() => {
        try {
            var client = await db.connect();
            var query = Todo.get();
            var { rows } = await client.query(query.toParam());
            assert.isNotEmpty(rows);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe actualizar el Todo por ID', async() => {
        try {
            var client = await db.connect();
            var query = Todo.updateByPk(todoid,{title:"test_update"});
            var { rows } = await client.query(query.toParam());
            assert.equal(rows[0].title,"test_update");
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe eliminar el Todo Todo por ID', async() => {
        try {
            var client = await db.connect();
            var query = Todo.removeByPk(todoid);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.isEmpty(rows);
            assert.equal(rowCount,1);
            // Eliminar usuario de prueba
            var query = User.removeByPk(userid);
            var { rows, rowCount } = await client.query(query.toParam());
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
  });