# Taller Diseño de API - Backend NodeJS with tdd

## Sistema de Blog

```
    /src
        /db                 Funciones de conexion de la BD
        /helpers            Funciones de Uso generico
        /routes             Acceso de los recursos
        /services           Modelos y validaciones para hacer operaciones en la BD
        /test               Modulo de pruebas
            /integration    Funciones de Integración de los recusros
            /unit           Funciones de Pruebas de funcionalidad
```