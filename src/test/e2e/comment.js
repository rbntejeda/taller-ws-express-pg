const { apiClient } = require('./config');
const { assert } = require('chai');
const resource = '/comments'

describe('comment API', () => {    
    describe('Creación',() => {
        var userid;
        it('Debe crear usuario', async () => {
            try {
                rand = Math.floor((Math.random() * 10000000000) + 1);
                var data = {
                    "name": "ruben"+rand,
                    "username": "rubentest"+rand,
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                const response = await apiClient
                    .post('/users')
                    .send(data)
                    .expect(201);
                const body = response.body;
                userid = body.id;
            } catch (err) {
                throw(err);
            }
        })
        var postid;
        it('Debe crear post', async () => {
            try {
                var { body } = await apiClient
                    .post('/posts')
                    .send({
                        "userid":userid,
                        "title":"HolaASKFASKFMASFLNASFN",
                        "body":"este es un bodnyldsnlaksngañsgnañslkgnasñlgnasñlASFÑLKADSGÑKDSJGÑAKNGAÑLSKFGNÑLASFDKNGÑSDFLKNGDFÑLKNDSFÑL<ÑLNDFLSDKNFÑSDLKNFÑASDLNngasñngasñngañskngsñ"
                    })
                    postid = body.id;
            } catch (err) {
                throw(err);
            }
        })

        const props = ['id','postid','name','email','body'];

        it('Debe rechazar sin datos', async() => {
            try {
                await apiClient
                    .post(resource)
                    .send({})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar con datos invalidos', async() => {
            try {
                await apiClient
                    .post(resource)
                    .send({
                        "postid":postid,
                        "name":"aañknfñlsdknfñas",
                        "email":"ruben@hofaknsflknasf.cl",
                        "body":"askjnaskfjaflk"
                    })
                    .expect(422);
            } catch (err) {
                throw(err);
            }
            
        })
        it('Debe crear comment', async () => {
            try {
                var { body } = await apiClient
                    .post(resource)
                    .send({
                        "postid":postid,
                        "name":"aañknfñlsdknfñasdnfñasdflkn",
                        "email":"ruben@hofaknsflknasf.cl",
                        "body":"askjnaskfjaflkasfkjbwdñgfnañowdgnañsldkngñalsdkngñlaksdngñlaksdngñasnkdgñaknsdñgnkadsnkg{asndg{asgn{sdn{asnd{asdng{ansdgsdnfañldsknfalsdknfañlsdkfnñlsdfknañsdfnñsdfnñsdlnfsñdlf"
                    })
                    .expect(201);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
            } catch (err) {
                throw(err);
            }
        })
    })
    
    const props = ['id','postid','name','email','body'];
    var commentid;
    describe('Editar', () => {        var userid;
        it('Debe crear usuario', async () => {
            try {
                rand = Math.floor((Math.random() * 10000000000) + 1);
                var data = {
                    "name": "ruben"+rand,
                    "username": "rubentest"+rand,
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                const response = await apiClient
                    .post('/users')
                    .send(data)
                    .expect(201);
                const body = response.body;
                userid = body.id;
            } catch (err) {
                throw(err);
            }
        })
        var postid;
        it('Debe crear post', async () => {
            try {
                var { body } = await apiClient
                    .post('/posts')
                    .send({
                        "userid":userid,
                        "title":"HolaASKFASKFMASFLNASFN",
                        "body":"este es un bodnyldsnlaksngañsgnañslkgnasñlgnasñlASFÑLKADSGÑKDSJGÑAKNGAÑLSKFGNÑLASFDKNGÑSDFLKNGDFÑLKNDSFÑL<ÑLNDFLSDKNFÑSDLKNFÑASDLNngasñngasñngañskngsñ"
                    })
                    postid = body.id;
            } catch (err) {
                throw(err);
            }
        })
        it('Debe crear comment', async () => {
            try {
                var { body } = await apiClient
                    .post(resource)
                    .send({
                        "postid":postid,
                        "name":"aañknfñlsdknfñasdnfñasdflkn",
                        "email":"ruben@hofaknsflknasf.cl",
                        "body":"askjnaskfjaflkasfkjbwdñgfnañowdgnañsldkngñalsdkngñlaksdngñlaksdngñasnkdgñaknsdñgnkadsnkg{asndg{asgn{sdn{asnd{asdng{ansdg"
                    })
                    .expect(201);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
                    commentid = body.id;
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar si la comment no existe', async() => {
            try {
                await apiClient
                    .put(resource+"/10000000")
                    .send({title:"test"})
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar editar sin datos', async() => {
            try {
                await apiClient
                    .put(resource+"/"+commentid)
                    .send({})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar editar con datos invalidos', async() => {
            try {
                await apiClient
                    .put(resource+"/"+commentid)
                    .send({body:"invalido"})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe editar', async() => {
            try {
                await apiClient
                    .put(resource+"/"+commentid)
                    .send({name:"Este tutlo es valido porque si"})
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
    })
    describe('Obtener', () => {
        it('Debe obtener todos los comments', async() => {
            try {
                var { body } = await apiClient
                    .get(resource)
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props);
            } catch (err) {
                throw(err);
            }
        })
        it('No debe rechazar comment que no existe', async() => {
            try {
                await apiClient
                    .get(resource+"/"+(commentid+100))
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener comment por ID', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+"/"+commentid)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
            } catch (err) {
                throw(err);
            }
        })
    })
    describe('Eliminación', () => {
        it('Debe rechazar eliminar si el comment no existe', async() => {
            try {
                await apiClient
                    .delete(resource+`/${commentid+100}`)
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe eliminar por ID', async() => {
            try {
                await apiClient
                    .delete(resource+`/${commentid}`)
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
    })
})