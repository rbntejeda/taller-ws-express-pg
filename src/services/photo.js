var squel = require("squel").useFlavour('postgres');
var _ = require("lodash");
var validate = require("validate.js");

var { fields } = require('../helpers');

const table = "photos";

exports.get = () => squel.select().from(table);
exports.insert = ({ albumid, title, url, thumbnailurl }) => fields(squel.insert().into(table),{ albumid, title, url, thumbnailurl });
exports.update = ({ albumid, title, url, thumbnailurl }) => fields(squel.update().table(table),{ albumid, title, url, thumbnailurl });
exports.remove = () => squel.delete().from(table);

exports.save = ({id, albumid, title, url, thumbnailurl }) => (id)?this.update({ albumid, title, url, thumbnailurl }).where('id = ?',id):this.insert({ albumid, title, url, thumbnailurl });

exports.getByPk = id => this.get().where('id = ?', id);
exports.removeByPk = id => this.remove().where("id = ?", id);
exports.updateByPk = ( id, model ) => this.update(model).where("id = ?", id);

exports.expand = async ( models, fields, client ) => {
    if(fields.includes("albums")){
        var uids =_.uniq(_.map(models,"albumid"));
        var { rows } = await client.query(Album.get().where("id IN ?",uids).toParam());
        _.each(models, x => x.post=_.find(rows,{id:x.postid}))
    }
}

exports.validate = ({ albumid, title, url, thumbnailurl }) => validate({ albumid, title, url, thumbnailurl },this.schema);

exports.schema = {
    albumid:{
        presence:true,
        numericality: {
            onlyInteger: true,
        }
    },
    title:{
        presence:true,
        length:{
            minimum:10,
            maximum:64
        }
    },
    url:{
        presence:true,
        length:{
            minimum:10,
            maximum:500
        },
        url: {
            schemes: ["https"]
        }
    },
    thumbnailurl:{
        presence:true,
        length:{
            minimum:10,
            maximum:64
        }
    }
}