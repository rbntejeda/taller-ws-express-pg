var router = require('express').Router();
var _ = require("lodash");

var { User, Address, Company, Post, Album, Todo } = require('../services');
var db = require('../db');


router.get('/', async function(req, res) {
    try {
        var client = await db.connect();
        var { fields, perPage, page } = req.query;
        var query = User.get();
        if(page||perPage){
            var { rows } = await client.query(query.clone().field('count(*)').toParam());
            var count = rows[0].count;
            page=page||1;
            perPage=perPage||10;
            res.setHeader('pagination-current-page',page);
            res.setHeader('pagination-per-page',perPage);
            res.setHeader('pagination-total-count',count);
            res.setHeader('pagination-total-page',Math.ceil(count/perPage));
            query.limit(perPage).offset((page-1)*perPage);
        }
        var { rows:users } = await client.query(query.toParam());
        if(users.length>0)
        {
            if(fields)
            {
                await User.expand(users,fields.split(','),client);
            }
            res.status(200).json(users);
        }
        res.status(404).send();
    } catch (error) {
        res.status(500).json(error);
    } finally {
        client.release();
    }
});

router.get('/:id', async function(req, res) {
    try {
        var client = await db.connect();
        var { fields } = req.query;
        var { id } = req.params;
        var query = User.getByPk(id);
        var { rows:users } = await client.query(query.toParam());
        if(users.length==1)
        {
            if(fields)
            {
                await User.expand(users,fields.split(','),client);
            }
            res.status(200).json(users[0]);
        }
        res.status(404).send();
    } catch (error) {
        res.status(500).json(error);
    } finally {
        client.release();
    }
});

router.get('/:id/:relation', async function(req, res) {
    try {
        var client = await db.connect();
        var { id, relation } = req.params;
        switch(relation)
        {
            case "posts":
            var query = Post.get();
            break;
            case "albums":
            var query = Album.get();
            break;
            case "todos":
            var query = Todo.get();
            break;
            default:
            throw("No existe esta relacion");
        }
        query.where("userid = ?",id);
        var { rows } = await client.query(query.toParam());
        if(rows.length>0)
        {
            res.status(200).json(rows);
        }
        else
        {
            res.status(404).send();
        }
    } catch (error) {
        res.status(500).json(error);
    } finally {
        client.release();
    }
});

router.post('/', async (req,res) => {     
    try {   
        var client = await db.connect();
        await client.query('BEGIN');
        if(req.body)
        {
            let userValidate = await User.validate(req.body,client);
            if(!req.body.address)
            {
                userValidate=userValidate||{};
                userValidate.address=["Address es requerido."]
            }
            if(!req.body.company)
            {
                userValidate=userValidate||{};
                userValidate.company=["Company es requerido."]
            }
            if(userValidate)
            {
                res.status(422).json(userValidate);
            }else
            {
                var { rows } = await client.query(User.insert(req.body).toParam());
                var user = rows[0];
                req.body.address.userid=user.id;
                let addressValidate = Address.validate(req.body.address);
                if(addressValidate)
                {
                    res.status(422).json({address:addressValidate});
                    await client.query('ROLLBACK');
                }
                else
                {
                    var { rows } = await client.query(Address.insert(req.body.address).toParam());
                    user.address = rows[0];
                    req.body.company.userid=user.id;
                    let companyValidate = Company.validate(req.body.company);
                    if(companyValidate)
                    {
                        await client.query('ROLLBACK');
                        res.status(422).json({company:companyValidate});
                    }
                    else
                    {
                        var { rows } = await client.query(Company.insert(req.body.company).toParam());
                        user.company = rows[0];
                        res.status(201).json(user);
                        await client.query('COMMIT');
                    }
                }
            }           
        }
        else
        {
            throw("No existe información")
        }
    } catch (error) {
        await client.query('ROLLBACK');
        res.status(500).json(error);
        throw error;
    } finally {
        client.release();
    }
})

router.put('/:id', async (req,res) => {
    try {
        var { body } = req;
        if(!_.isEmpty(body))
        {
            var client = await db.connect();
            var { id } = req.params; 
            var { rows } = await client.query(User.getByPk(id).toParam());
            if(!_.isEmpty(rows))
            {
                _.assign(rows[0],body);
                var validate = await User.validate(rows[0],client);
                if(validate)
                {
                    res.status(422).send(validate);
                }
                else
                {
                    var { rows } = await client.query(User.updateByPk(id,body).toParam());
                    res.status(200).send(rows[0]);
                }
            }
            else
            {
                res.status(404).send();
            }
        }
        else
        {
            res.status(422).send("No existe información a actualizar.");
        }
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

router.delete('/:id',async (req,res) => {
    try {
        
        var client = await db.connect();
        const { rows } = await client.query(User.getByPk(req.params.id).toParam());
        if( rows.length > 0)
        {
            await client.query(User.removeByPk(req.params.id).toParam());
            res.status(200).send();
        }
        else
        {
            res.status(404).send();            
        }
    } catch (error) {
        res.status(500).json(error);
    } finally
    {
        client.release();
    }
})

module.exports = router;