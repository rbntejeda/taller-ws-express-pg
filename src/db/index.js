var { Pool } = require('pg');

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'blog',
    password: '164352',
});

exports.connect = async () => {
    return await pool.connect();   
};