const { assert } = require("chai");
const pacientesReporsitorio = require("./../../repositorio/pacientes");

describe('Repositorio Paciente', () => {
    let pacienteId;
    const props = ['id','nombre','apellidos','edad'];
    it('Debe guardar un paciente', async () => {
        const data = {
            nombre : "Ruben",
            apellidos : "Tejeda",
            edad : 26
        }
        try {
            const paciente = await pacientesReporsitorio.save(data);
            assert.isObject(paciente);
            assert.hasAllKeys(paciente, props);
            pacienteId = paciente.id;
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    });
    it('Debe obtener un paciente por ID', async () => {
        try {
            const paciente = await pacientesReporsitorio.getById(pacienteId);
            assert.isObject(paciente);
            assert.hasAllKeys(paciente, props);
            pacienteId = paciente.id;
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    });
    it('Debe obtener todos los pacientes', async () => {
        try {
            const paciente = await pacientesReporsitorio.getAll();
            assert.isArray(paciente);
            assert.isNotEmpty(paciente);
            assert.hasAllKeys(paciente[0],props);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    });

});