var { assert } = require("chai");

var { User, Address } = require("../../services");

var db = require("../../db");

describe('Address', function() {
    let userId;
    const props = ['id','userid','street','suite','city','zipcode','lat','lng'];
    let addressId;
    it('Debe guardar un Address', async () => {
        try {
            var client = await db.connect();
            //Agrega información previa
            var query = User.insert({
                name:"ruben",
                username:"rubentest",
                email:"ruben@ruben.cl",
                phone:"9123345",
                website:"ruben.cl",
            });
            var { rows } = await client.query(query.toParam());
            userId = rows[0].id;

            var query = Address.insert({
                userid:userId,
                street:"calle seis",
                suite:"2344",
                city:"conepción",
                zipcode:21.124532432,
                lat:0.432524654,
                lng:0.3253263463
            });
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
            addressId = rows[0].id;
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener el Address por ID', async() => {
        try {
            var client = await db.connect();
            var query = Address.getByPk(addressId);
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener todos los Addresss', async() => {
        try {
            var client = await db.connect();
            var query = Address.get();
            var { rows } = await client.query(query.toParam());
            assert.isNotEmpty(rows);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe actualizar el Address por ID', async() => {
        try {
            var client = await db.connect();
            var query = Address.updateByPk(addressId,{street:"test_update"});
            var { rows } = await client.query(query.toParam());
            assert.equal(rows[0].street,"test_update");
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe eliminar el Address Address por ID', async() => {
        try {
            var client = await db.connect();
            var query = Address.removeByPk(addressId);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.isEmpty(rows);
            assert.equal(rowCount,1);
            // Eliminar usuario de prueba
            var query = User.removeByPk(userId);
            var { rows, rowCount } = await client.query(query.toParam());
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
  });