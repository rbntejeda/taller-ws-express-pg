var squel = require("squel").useFlavour('postgres');
var _ = require("lodash");
var validate = require("validate.js");

var { fields } = require('../helpers');
var User = require("../services/user");

const table = "todos";

exports.get = () => squel.select().from(table);
exports.insert = ({ userid, title, completed }) => fields(squel.insert().into(table),{ userid, title, completed });
exports.update = ({ userid, title, completed }) => fields(squel.update().table(table),{ userid, title, completed });
exports.remove = () => squel.delete().from(table);

exports.save = ({id, userid, title, completed }) => (id)?this.update({ userid, title, completed }).where('id = ?',id):this.insert({ userid, title, completed });

exports.getByPk = id => this.get().where('id = ?', id);
exports.removeByPk = id => this.remove().where("id = ?", id);
exports.updateByPk = ( id, model ) => this.update(model).where("id = ?", id);

exports.expand = async ( models, fields, client ) => {
    if(fields.includes("user")){
        var uids =_.uniq(_.map(models,"userid"));
        var { rows } = await client.query(User.get().where("id IN ?",uids).toParam());
        _.each(models, x => x.user=_.find(rows,{id:x.userid}))
    }
}

exports.validate = ({ userid, title, completed }) => validate({ userid, title, completed },this.schema);

exports.schema = {
    userid:{
        presence:true,
        numericality: {
            onlyInteger: true,
        }
    },
    title:{
        presence:true,
        length:{
            minimum:10,
            maximum:64
        }
    },
    completed:{
        presence:true,
        inclusion: {
            within: [true,false],
            message: "^Debe ser true o false"
        }
    }
}