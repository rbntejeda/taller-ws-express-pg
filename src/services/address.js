var squel = require("squel").useFlavour('postgres');
var validate = require('validate.js');
var { fields } = require('../helpers');

const table = "addresses";

exports.get = () => squel.select().from(table);
exports.insert = ({ userid, street, suite, city, zipcode, lat, lng }) => fields(squel.insert().into(table),{ userid, street, suite, city, zipcode, lat, lng });
exports.update = ({ userid, street, suite, city, zipcode, lat, lng }) => fields(squel.update().table(table),{ userid, street, suite, city, zipcode, lat, lng });
exports.remove = () => squel.delete().from(table);

exports.save = ({id, userid, street, suite, city, zipcode, lat, lng }) => (id)?this.update({ userid, street, suite, city, zipcode, lat, lng }).where('id = ?',id):this.insert({ userid, street, suite, city, zipcode, lat, lng });

exports.getByPk = id => this.get().where('id = ?', id);
exports.removeByPk = id => this.remove().where("id = ?", id);
exports.updateByPk = ( id, model ) => this.update(model).where("id = ?", id);

exports.validate = ({ id, userid, street, suite, city, zipcode, lat, lng }) => validate({ id, userid, street, suite, city, zipcode, lat, lng },this.schema);

exports.schema = {
    userid:{
        presence:true,
        numericality: {
            onlyInteger: true
        }
    },
    street:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        }
    },
    suite:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        }
    },
    city:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        }
    },
    zipcode:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        }
    },
    lat:{
        presence:true,
        numericality:true
    },
    lng:{
        presence:true,
        numericality:true
    }
}