const { apiClient } = require('./config');
const { assert } = require('chai');
const resource = '/users'

describe('User API', () => {    
    const props = ['id','name','username','email','phone','website'];
    describe('Creación',() => {
        it('Debe rechazar sin datos', async() => {
            try {
                await apiClient
                    .post(resource)
                    .send({})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar con datos invalidos', async() => {
            try {
                var data = {
                    "name": "ruben",
                    "username": "rubentest",
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                await apiClient
                    .post(resource)
                    .send(data)
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        var userid;
        it('Debe crear', async () => {
            try {
                rand = Math.floor((Math.random() * 10000000000) + 1);
                var data = {
                    "name": "ruben"+rand,
                    "username": "rubentest"+rand,
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                const response = await apiClient
                    .post(resource)
                    .send(data)
                    .expect(201);
                const body = response.body;
                assert.isObject(body);
                assert.hasAllKeys(body, (props.concat(['address','company'])));
                userid = body.userid;
            } catch (err) {
                throw(err);
            }
        })
    })
        var userid;

    describe('Editar', () => {
        it('Debe crear', async () => {
            try {
                rand = Math.floor((Math.random() * 10000000000) + 1);
                var data = {
                    "name": "ruben"+rand,
                    "username": "rubentest"+rand,
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                const response = await apiClient
                    .post(resource)
                    .send(data)
                    .expect(201);
                const body = response.body;
                assert.isObject(body);
                assert.hasAllKeys(body, (props.concat(['address','company'])));
                userid = body.id;
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar si el usuario no existe', async() => {
            try {
                await apiClient
                    .put(resource+"/10000000")
                    .send({name:"test"})
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar editar sin datos', async() => {
            try {
                await apiClient
                    .put(resource+"/"+userid)
                    .send({})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar editar con datos invalidos', async() => {
            try {
                await apiClient
                    .put(resource+"/"+userid)
                    .send({name:"test"})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe editar', async() => {
            try {
                await apiClient
                    .put(resource+"/"+userid)
                    .send({username:"ruben1234567"})
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
    })
    describe('Obtener', () => {
        it('Debe obtener todos los usuarios', async() => {
            try {
                var { body } = await apiClient
                    .get(resource)
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener todos los usuarios junto a su company', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+"?fields=company")
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props.concat(['company']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener todos los usuarios junto a su address', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+"?fields=address")
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props.concat(['address']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener todos los usuarios junto a sus posts', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+"?fields=posts")
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props.concat(['posts']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener todos los usuarios junto a sus albums', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+"?fields=albums")
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props.concat(['albums']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener todos los usuarios junto a sus todos', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+"?fields=todos")
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props.concat(['todos']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener todos los usuarios junto a sus company,address,posts,albums,todos', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+"?fields=company,address,posts,albums,todos")
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props.concat(['company','address','posts','albums','todos']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener usuario por ID', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}`)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener usuario por ID junto a su company', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}?fields=company`)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props.concat(['company']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener usuario por ID junto a su address', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}?fields=address`)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props.concat(['address']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener usuario por ID junto a sus posts', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}?fields=posts`)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props.concat(['posts']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener usuario por ID junto a sus albums', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}?fields=albums`)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props.concat(['albums']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener usuario por ID junto a sus todos', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}?fields=todos`)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props.concat(['todos']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener usuario por ID junto a sus company,address,posts,albums,todos', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}?fields=company,address,posts,albums,todos`)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props.concat(['company','address','posts','albums','todos']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar si no tiene posts por el ID del usuario', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}/posts`)
                    .expect(404);
                    // assert.isObject(body[0]);
                    // assert.hasAllKeys(body, props.concat(['company','address','posts','albums','todos']));
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar si no tiene albums por el ID del usuario', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}/albums`)
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar si no tiene todos por el ID del usuario', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}/todos`)
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener todos los posts por el ID del usuario', async() => {
            try {
                var data = {
                    userid:userid,
                    title:"Este es un titulo de prueba flkmasf",
                    body:"Este es el contenido asdasfnasfoasjfaosnfapsfnapsofnasñkmldkmfñaslkdmfñasdkmfñasdfmaañsmfasfkmasñfkmasñfkmasñfkmasñkfmsa"
                };
                const response = await apiClient
                    .post('/posts')
                    .send(data)
                    .expect(201);
                var { body } = await apiClient
                    .get(resource+`/${userid}/posts`)
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener todos los albums por el ID del usuario', async() => {
            try {                
                var data = {
                    userid:userid,
                    title:"Este es un titulo de prueba flkmasf",
                };
                const response = await apiClient
                    .post('/albums')
                    .send(data)
                    .expect(201);
                var { body } = await apiClient
                    .get(resource+`/${userid}/albums`)
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener todos los todos por el ID del usuario', async() => {
            try {
                var data = {
                    userid:userid,
                    title:"Este es un titulo de prueba flkmasf",
                    completed:true
                };
                const response = await apiClient
                    .post('/todos')
                    .send(data)
                    .expect(201);
                var { body } = await apiClient
                    .get(resource+`/${userid}/todos`)
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })        
        it('Debe rechazar usuario cuyo ID no existe', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+`/${userid}00/todos`)
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
    })
    describe('Eliminación', () => {
        var userid;
        it('Debe crear', async () => {
            try {
                rand = Math.floor((Math.random() * 10000000000) + 1);
                var data = {
                    "name": "ruben"+rand,
                    "username": "rubentest"+rand,
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                const response = await apiClient
                    .post(resource)
                    .send(data)
                    .expect(201);
                const body = response.body;
                assert.isObject(body);
                assert.hasAllKeys(body, (props.concat(['address','company'])));
                userid = body.id;
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar eliminar si usuario no existe', async() => {
            try {
                await apiClient
                    .delete(resource+`/${userid+100}`)
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe eliminar por ID', async() => {
            try {
                await apiClient
                    .delete(resource+`/${userid}`)
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
    })
})