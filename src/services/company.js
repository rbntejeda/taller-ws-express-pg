var squel = require("squel").useFlavour('postgres');
var validate = require('validate.js');

var { fields } = require('../helpers');

const table = "companies";

exports.get = () => squel.select().from(table);
exports.insert = ({ userid, name, catchphrase, bs}) => fields(squel.insert().into(table),{ userid, name, catchphrase, bs});
exports.update = ({ userid, name, catchphrase, bs}) => fields(squel.update().table(table),{ userid, name, catchphrase, bs});
exports.remove = id => squel.delete().from(table);

exports.save = ({ id, userid, name, catchphrase, bs}) => (id)?this.update({ userid, name, catchphrase, bs}).where('id = ?',id):this.insert({ userid, name, catchphrase, bs});

exports.validate = ({ userid, name, catchphrase, bs}) => validate({ userid, name, catchphrase, bs},this.schema);

exports.getByPk = id => this.get().where('id = ?', id);
exports.removeByPk = id => this.remove().where("id = ?", id);
exports.updateByPk = ( id, model ) => this.update(model).where("id = ?", id);


exports.schema = {
    userid:{
        presence:true,
        numericality: {
            onlyInteger: true
        }
    },
    name:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        }
    },
    catchphrase:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        }
    },
    bs:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        }
    }
}
