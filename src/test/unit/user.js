var { assert } = require("chai");

var { User } = require("../../services");

var db = require("../../db");

describe('User', function() {
    const props = ['id','name','username','email','phone','website'];
    let userId;
    it('Debe guardar un usuario', async () => {
        try {
            var client = await db.connect();
            var query = User.insert({
                name:"ruben",
                username:"rubentest",
                email:"ruben@ruben.cl",
                phone:"9123345",
                website:"ruben.cl",
            });
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
            userId = rows[0].id;
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener el usuario por ID', async() => {
        try {
            var client = await db.connect();
            var query = User.getByPk(userId);
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener todos los usuarios', async() => {
        try {
            var client = await db.connect();
            var query = User.get();
            var { rows } = await client.query(query.toParam());
            assert.isNotEmpty(rows);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe actualizar el usuario por ID', async() => {
        try {
            var client = await db.connect();
            var query = User.updateByPk(userId,{username:"test_update"});
            var { rows } = await client.query(query.toParam());
            assert.equal(rows[0].username,"test_update");
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe eliminar el usuario usuario por ID', async() => {
        try {
            var client = await db.connect();
            var query = User.removeByPk(userId);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.isEmpty(rows);
            assert.equal(rowCount,1);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
  });