var squel = require("squel").useFlavour('postgres');
var validate = require('validate.js');
var _ = require('lodash');

validate.validators.presence.options= {
    message:"no puede estar vacío."
}
validate.validators.url.options= {
    message:"no es una url valida."
}
validate.validators.length.options= {
    tooShort:"es muy corto (el minimo es %{count} de caracteres)",
    tooLong:"es muy largo (el maximo es %{count} de caracteres)"
}

var Post = require('./post');
var Album = require('./album');
var Todo = require('./todo');
var Address = require('./address');
var Company = require('./company');

var { fields } = require('../helpers');

const table = "users";

exports.get = () => squel.select().from(table);
exports.insert = ({name,username,email,phone,website}) => fields(squel.insert().into(table),{name,username,email,phone,website});
exports.update = ({name,username,email,phone,website}) => fields(squel.update().table(table),{name,username,email,phone,website});
exports.remove = () => squel.delete().from(table);

exports.getByPk = id => this.get().where('id = ?', id);
exports.removeByPk = id => this.remove().where("id = ?", id);
exports.updateByPk = ( id, model ) => this.update(model).where("id = ?", id);

exports.save = ({id,name,username,email,phone,website}) => (id)?this.update({name,username,email,phone,website}).where('id = ?',id):this.insert({name,username,email,phone,website});

exports.expand = async( arr, fields, client) => {
    var fields = _.map( fields, (v) => v.split('.') );
    var pks = _.map(arr,'id');
    await Promise.all(_.map(fields, async (v) => {
        switch(v[0])
        {
            case 'posts':
                var {rows} = await client.query(Post.get().where('userid IN ?',pks).toString());
                _.map(arr, e => e.posts = _.filter(rows,{userid:e.id}));
                break;
            case 'albums':
                var {rows} = await client.query(Album.get().where('userid IN ?',pks).toString());
                _.map(arr, e => e.albums = _.filter(rows,{userid:e.id}));
                break;
            case 'todos':
                var {rows} = await client.query(Todo.get().where('userid IN ?',pks).toString());
                _.map(arr, e => e.todos = _.filter(rows,{userid:e.id}));
                break;
            case 'address':
                var {rows} = await client.query(Address.get().where('userid IN ?',pks).toString());
                _.map(arr, e => e.address = _.find(rows,{userid:e.id}))||null;
                break;
            case 'company':    
                var {rows} = await client.query(Company.get().where('userid IN ?',pks).toString());
                _.map(arr, e => e.company = _.find(rows,{userid:e.id}))||null;
                break;
            default:
                break;
        }
    }))
}

exports.schema = {
    name:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        }
    },
    username:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        }
    },
    email:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        },
        email:true,
        format:{
            pattern:/\S+\b([.]cl|[.]com)\b/i,
            message:"debe terminar en .cl o .com"
        }
    },
    phone:{
        presence:true,
        format:{
            pattern:/[0-9\-]+/,
            message:"solo puede contener numeros y guiones."
        },
        length:{
            minimum:10,
            maximum:30
        }
    },
    website:{
        presence:true,
        length:{
            minimum:10,
            maximum:50
        },
        url:true
    }
}

exports.validate = async ({ id, name, username, email, phone, website },client) => {
    var invalid = validate({ id, name, username, email, phone, website },this.schema);
    if(id===undefined)
    {
        var { rowCount } = await client.query(this.get().where('username = ?',username).limit(1).toParam());
        if(rowCount)
        {
            invalid=invalid||{},
            invalid.username = invalid.username||[];
            invalid.username.push("Username ya se encuentra en uso");
        }
    }
    return invalid;
}