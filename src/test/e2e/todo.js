const { apiClient } = require('./config');
const { assert } = require('chai');
const resource = '/todos'

describe('Todo API', () => {    
    describe('Creación',() => {
        var userid;
        it('Debe crear', async () => {
            try {
                rand = Math.floor((Math.random() * 10000000000) + 1);
                var data = {
                    "name": "ruben"+rand,
                    "username": "rubentest"+rand,
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                const response = await apiClient
                    .post('/users')
                    .send(data)
                    .expect(201);
                const body = response.body;
                userid = body.id;
            } catch (err) {
                throw(err);
            }
        })

        const props = ['id','userid','title','completed'];

        it('Debe rechazar sin datos', async() => {
            try {
                await apiClient
                    .post(resource)
                    .send({})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar con datos invalidos', async() => {
            try {
                await apiClient
                    .post(resource)
                    .send({
                        userid:userid,
                        title:"Hola",
                        completed:false
                    })
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe crear', async () => {
            try {
                var { body } = await apiClient
                    .post(resource)
                    .send({
                        userid:userid,
                        title:"Este es una tarea valida se enteiende porque",
                        completed:false
                    })
                    .expect(201);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
            } catch (err) {
                throw(err);
            }
        })
    })
    
    const props = ['id','userid','title','completed'];
    var todoid;
    describe('Editar', () => {        var userid;
        it('Debe crear usuario', async () => {
            try {
                rand = Math.floor((Math.random() * 10000000000) + 1);
                var data = {
                    "name": "ruben"+rand,
                    "username": "rubentest"+rand,
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                const response = await apiClient
                    .post('/users')
                    .send(data)
                    .expect(201);
                const body = response.body;
                userid = body.id;
            } catch (err) {
                throw(err);
            }
        })

        it('Debe crear tarea', async () => {
            try {
                var { body } = await apiClient
                    .post(resource)
                    .send({
                        userid:userid,
                        title:"Este es una tarea valida se enteiende porque",
                        completed:false
                    })
                    .expect(201);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
                    todoid = body.id;
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar si la tarea no existe', async() => {
            try {
                await apiClient
                    .put(resource+"/10000000")
                    .send({title:"test"})
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar editar sin datos', async() => {
            try {
                await apiClient
                    .put(resource+"/"+todoid)
                    .send({})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar editar con datos invalidos', async() => {
            try {
                await apiClient
                    .put(resource+"/"+todoid)
                    .send({title:"invalido"})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe editar', async() => {
            try {
                await apiClient
                    .put(resource+"/"+todoid)
                    .send({title:"Este tutlo es valido porque si"})
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
    })
    describe('Obtener', () => {
        it('Debe obtener todos los usuarios', async() => {
            try {
                var { body } = await apiClient
                    .get(resource)
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props);
            } catch (err) {
                throw(err);
            }
        })
        it('No debe obtener usuario cuyo ID no existe', async() => {
            try {
                await apiClient
                    .get(resource+"/"+(todoid+100))
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener usuario por ID', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+"/"+todoid)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
            } catch (err) {
                throw(err);
            }
        })
    })
    describe('Eliminación', () => {
        it('Debe rechazar eliminar si usuario no existe', async() => {
            try {
                await apiClient
                    .delete(resource+`/${todoid+100}`)
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe eliminar por ID', async() => {
            try {
                await apiClient
                    .delete(resource+`/${todoid}`)
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
    })
})