var router = require('express').Router();
var _ = require("lodash");

var db = require('../db');

var { Comment } = require('../services');

router.get('/', async function(req, res) {
    try {
        var client = await db.connect();
        var { fields, perPage, page } = req.query;
        var query = Comment.get();
        if(page||perPage){
            var { rows } = await client.query(query.clone().field('count(*)').toParam());
            var count = rows[0].count;
            page=page||1;
            perPage=perPage||10;
            res.setHeader('pagination-current-page',page);
            res.setHeader('pagination-per-page',perPage);
            res.setHeader('pagination-total-count',count);
            res.setHeader('pagination-total-page',Math.ceil(count/perPage));
            query.limit(perPage).offset((page-1)*perPage);
        }
        var { rows:models } = await client.query(query.toParam());
        if(models.length>0)
        {
            if(fields)
            {
                await Comment.expand(models,fields.split(','),client);
            }
            res.status(200).json(models);
        }
        res.status(404).send();
    } catch (error) {
        res.status(500).json(error);
    } finally {
        client.release();
    }
});

router.get('/:id', async function(req, res) {
    try {
        var client = await db.connect();
        var { fields } = req.query;
        var { id } = req.params;
        var query = Comment.getByPk(id);
        var { rows } = await client.query(query.toParam());
        if(rows.length==1)
        {
            if(fields)
            {
                await Comment.expand(rows,fields.split(','),client);
            }
            res.status(200).json(rows[0]);
        }
        res.status(404).send();
    } catch (error) {
        res.status(500).json(error);
    } finally {
        client.release();
    }
});

router.post('/', async (req,res) => {     
    try {   
        var client = await db.connect();
        var { body } = req;
        var validate = Comment.validate(body);
        if(validate)
        {
            res.status(422).json(validate);
        }
        else
        {
            var { rows } = await client.query(Comment.insert(body).toParam());
            res.status(201).json(rows[0]);
        }
    } catch (error) {
        res.status(500).json(error);
        throw error;
    } finally {
        client.release();
    }
})

router.put('/:id', async (req,res) => {
    try {
        var { body } = req;
        if(!_.isEmpty(body))
        {
            var client = await db.connect();
            var { id } = req.params; 
            var { rows } = await client.query(Comment.getByPk(id).toParam());
            if(rows.length==1)
            {
                _.assign(rows[0],body);
                var validate = Comment.validate(rows[0],client);
                if(validate)
                {
                    res.status(422).send(validate);
                }
                else
                {
                    var { rows } = await client.query(Comment.updateByPk(id,body).toParam());
                    res.status(200).send(rows[0]);
                }
            }
            else
            {
                res.status(404).send();
            }
        }
        else
        {
            res.status(422).send("No existe información a actualizar.");
        }
    } catch (error) {
        res.status(500).json(error);
    }
})

router.delete('/:id',async (req,res) => {
    try {
        var client = await db.connect();
        var { id } = req.params;
        const { rows } = await client.query(Comment.getByPk(id).toParam());
        if( rows.length > 0)
        {
            await client.query(Comment.removeByPk(id).toParam());
            res.status(200).send();
        }
        else
        {
            res.status(404).send();            
        }
    } catch (error) {
        res.status(500).json(error);
    } finally
    {
        client.release();
    }
})

module.exports = router;