var { assert } = require("chai");

var { User, Post } = require("../../services");

var db = require("../../db");

describe('Post', function() {
    let userId;
    const props = ['id','userid','title','body'];
    let postId;
    it('Debe guardar un post', async () => {
        try {
            var client = await db.connect();
            //Agrega información previa
            var query = User.insert({
                name:"ruben",
                username:"rubentest",
                email:"ruben@ruben.cl",
                phone:"9123345",
                website:"ruben.cl",
            });
            var { rows } = await client.query(query.toParam());
            userId = rows[0].id;

            var query = Post.insert({
                userid:userId,
                title:"Titulo test",
                body:"Este es el contenido"
            });
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
            postId = rows[0].id;
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener el post por ID', async() => {
        try {
            var client = await db.connect();
            var query = Post.getByPk(postId);
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener todos los posts', async() => {
        try {
            var client = await db.connect();
            var query = Post.get();
            var { rows } = await client.query(query.toParam());
            assert.isNotEmpty(rows);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe actualizar el post por ID', async() => {
        try {
            var client = await db.connect();
            var query = Post.updateByPk(postId,{title:"test_update"});
            var { rows } = await client.query(query.toParam());
            assert.equal(rows[0].title,"test_update");
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe eliminar el post post por ID', async() => {
        try {
            var client = await db.connect();
            var query = Post.removeByPk(postId);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.isEmpty(rows);
            assert.equal(rowCount,1);
            // Eliminar usuario de prueba
            var query = User.removeByPk(userId);
            var { rows, rowCount } = await client.query(query.toParam());
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
  });