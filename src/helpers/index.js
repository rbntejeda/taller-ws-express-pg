var { pickBy } = require('lodash');

exports.clean = model => pickBy(model, m => m !==undefined);

exports.fields = (sql,data) => sql.setFields(this.clean(data)).returning('*');