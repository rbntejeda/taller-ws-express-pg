var { assert } = require("chai");

var { User, Company } = require("../../services");

var db = require("../../db");

describe('Company', function() {
    let userid;
    const props = ['id','userid','name','catchphrase','bs'];
    let companyid;
    it('Debe guardar un Company', async () => {
        try {
            var client = await db.connect();
            //Agrega información previa
            var query = User.insert({
                name:"ruben",
                username:"rubentest",
                email:"ruben@ruben.cl",
                phone:"9123345",
                website:"ruben.cl",
            });
            var { rows } = await client.query(query.toParam());
            userid = rows[0].id;

            var query = Company.insert({
                userid:userid,
                name:"Qualitat test",
                catchphrase:"Nose es un test",
                bs:"esto es algo"
            });
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
            companyid = rows[0].id;
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener el Company por ID', async() => {
        try {
            var client = await db.connect();
            var query = Company.getByPk(companyid);
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener todos los Companys', async() => {
        try {
            var client = await db.connect();
            var query = Company.get();
            var { rows } = await client.query(query.toParam());
            assert.isNotEmpty(rows);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe actualizar el Company por ID', async() => {
        try {
            var client = await db.connect();
            var query = Company.updateByPk(companyid,{name:"test_update"});
            var { rows } = await client.query(query.toParam());
            assert.equal(rows[0].name,"test_update");
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe eliminar el Company Company por ID', async() => {
        try {
            var client = await db.connect();
            var query = Company.removeByPk(companyid);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.isEmpty(rows);
            assert.equal(rowCount,1);
            // Eliminar usuario de prueba
            var query = User.removeByPk(userid);
            var { rows, rowCount } = await client.query(query.toParam());
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
  });