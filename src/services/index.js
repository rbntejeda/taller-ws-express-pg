exports.User = require('./user');
exports.Post = require('./post');
exports.Comment = require('./comment');
exports.Album = require('./album');
exports.Photo = require('./photo');
exports.Todo = require('./todo');
exports.Address = require('./address');
exports.Company = require('./company');