var { assert } = require("chai");

var { User, Album } = require("../../services");

var db = require("../../db");

describe('Album', function() {
    let userId;
    const props = ['id','userid','title'];
    let AlbumId;
    it('Debe guardar un Album', async () => {
        try {
            var client = await db.connect();
            //Agrega información previa
            var query = User.insert({
                name:"ruben",
                username:"rubentest",
                email:"ruben@ruben.cl",
                phone:"9123345",
                website:"ruben.cl",
            });
            var { rows } = await client.query(query.toParam());
            userId = rows[0].id;

            var query = Album.insert({
                userid:userId,
                title:"Titulo test"
            });
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
            AlbumId = rows[0].id;
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener el Album por ID', async() => {
        try {
            var client = await db.connect();
            var query = Album.getByPk(AlbumId);
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener todos los Albums', async() => {
        try {
            var client = await db.connect();
            var query = Album.get();
            var { rows } = await client.query(query.toParam());
            assert.isNotEmpty(rows);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe actualizar el Album por ID', async() => {
        try {
            var client = await db.connect();
            var query = Album.updateByPk(AlbumId,{title:"test_update"});
            var { rows } = await client.query(query.toParam());
            assert.equal(rows[0].title,"test_update");
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe eliminar el Album Album por ID', async() => {
        try {
            var client = await db.connect();
            var query = Album.removeByPk(AlbumId);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.isEmpty(rows);
            assert.equal(rowCount,1);
            // Eliminar usuario de prueba
            var query = User.removeByPk(userId);
            var { rows, rowCount } = await client.query(query.toParam());
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
  });