const bodyParser = require('body-parser');

module.exports = (app) => {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));

    app.use('/users', require('./user'));
    app.use('/posts', require('./post'));
    app.use('/comments', require('./comment'));
    app.use('/albums', require('./album'));
    app.use('/photos', require('./photo'));
    app.use('/todos', require('./todo'));
}