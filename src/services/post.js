var squel = require("squel").useFlavour('postgres');
var validate = require("validate.js");
var _ = require("lodash");

var { fields } = require('../helpers');

var User = require("./user");
var Comment = require("./comment");

const table = "posts";

exports.get = () => squel.select().from(table);
exports.insert = ({ userid, title, body}) => fields(squel.insert().into(table),{ userid, title, body});
exports.update = ({ userid, title, body}) => fields(squel.update().table(table),{ userid, title, body});
exports.remove = id => squel.delete().from(table);

exports.save = ({id, userid, title, body}) => (id)?this.update({ userid, title, body}).where('id = ?', id):this.insert({ userid, title, body});

exports.getByPk = id => this.get().where('id = ?', id);
exports.removeByPk = id => this.remove().where("id = ?", id);
exports.updateByPk = ( id, model ) => this.update(model).where("id = ?", id);

exports.expand = async ( models, fields, client ) => {
    var pks = _.map( models, 'id' );
    if(fields.includes("user")){
        var uids =_.uniq(_.map(models,"userid"));
        var { rows } = await client.query(User.get().where("id IN ?",uids).toParam());
        _.each(models, x => x.user=_.find(rows,{id:x.userid}))
    }
    if(fields.includes("comments")){
        var { rows } = await client.query(Comment.get().where("postid IN ?",pks).toParam());
        _.each(models, x => x.comments=_.filter(rows,{postid:x.id}))
    }
}

exports.validate = ({ userid, title, body}) => validate({ userid, title, body},this.schema);

exports.schema = {
    userid:{
        presence:true,
        numericality: {
            onlyInteger: true,
        }
    },
    title:{
        presence:true,
        length:{
            minimum:10,
            maximum:64
        }
    },
    body:{
        presence:true,
        length:{
            minimum:100,
            maximum:500
        }
    }
}