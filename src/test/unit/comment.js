var { assert } = require("chai");

var { User, Post, Comment } = require("../../services");

var db = require("../../db");

describe('Comment', function() {
    let userId;
    const props = ['id','postid','name','email','body'];
    let commentId;
    it('Debe guardar un Comment', async () => {
        try {
            var client = await db.connect();
            //Agrega información previa
            var query = User.insert({
                name:"ruben_test2",
                username:"rubentest",
                email:"ruben@ruben.cl",
                phone:"9123345",
                website:"ruben.cl",
            });
            var { rows } = await client.query(query.toParam());
            userid = rows[0].id;
            
            //agrega un post
            var query = Post.insert({
                userid:userid,
                title:"Titulo test",
                body:"Este es el contenido"
            });
            var { rows } = await client.query(query.toParam());            
            postid = rows[0].id;

            var query = Comment.insert({
                postid:postid,
                name:"Titulo test",
                email:"test@test.cl",
                body:"Este es el contenido"
            });
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
            commentId = rows[0].id;
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener el Comment por ID', async() => {
        try {
            var client = await db.connect();
            var query = Comment.getByPk(commentId);
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener todos los Comments', async() => {
        try {
            var client = await db.connect();
            var query = Comment.get();
            var { rows } = await client.query(query.toParam());
            assert.isNotEmpty(rows);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe actualizar el Comment por ID', async() => {
        try {
            var client = await db.connect();
            var query = Comment.updateByPk(commentId,{body:"test_update"});
            var { rows } = await client.query(query.toParam());
            assert.equal(rows[0].body,"test_update");
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe eliminar el Comment Comment por ID', async() => {
        try {
            var client = await db.connect();
            var query = Comment.removeByPk(commentId);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.isEmpty(rows);
            assert.equal(rowCount,1);
            // Eliminar usuario de prueba
            var query = User.removeByPk(userid);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.equal(rowCount,1);

        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
  });