var squel = require("squel").useFlavour('postgres');
var _ = require("lodash");
var validate = require("validate.js");

var { fields } = require('../helpers');
var User = require("../services/user");
var Photo = require("../services/photo");

const table = "albums";

exports.get = () => squel.select().from(table);
exports.insert = ({ userid, title }) => fields(squel.insert().into(table),{ userid, title });
exports.update = ({ userid, title }) => fields(squel.update().table(table),{ userid, title });
exports.remove = () => squel.delete().from(table);

exports.save = ({ id, userid, title}) => (id)?this.update({ userid, title }).where('id = ?',id):this.insert({ userid, title });

exports.getByPk = id => this.get().where('id = ?', id);
exports.removeByPk = id => this.remove().where("id = ?", id);
exports.updateByPk = ( id, model ) => this.update(model).where("id = ?", id);

exports.expand = async ( models, fields, client ) => {
    var pks = _.map( models, 'id' );
    if(fields.includes("user")){
        var uids =_.uniq(_.map(models,"userid"));
        var { rows } = await client.query(User.get().where("id IN ?",uids).toParam());
        _.each(models, x => x.user=_.find(rows,{id:x.userid}))
    }
    if(fields.includes("photos")){
        var { rows } = await client.query(Photo.get().where("albumid IN ?",pks).toParam());
        _.each(models, x => x.photos=_.filter(rows,{albumid:x.id}))
    }
}

exports.validate = ({ userid, title }) => validate({ userid, title },this.schema);

exports.schema = {
    userid:{
        presence:true,
        numericality: {
            onlyInteger: true,
        }
    },
    title:{
        presence:true,
        length:{
            minimum:10,
            maximum:64
        }
    }
}