var squel = require("squel").useFlavour('postgres');
var _ = require("lodash");
var validate = require("validate.js");

var { fields } = require('../helpers');

var Post = require("../services/post");

const table = "comments";

exports.get = () => squel.select().from(table);
exports.remove = () => squel.delete().from(table);
exports.insert = ({ postid, name, email, body }) => fields(squel.insert().into(table),{ postid, name, email, body });
exports.update = ({ postid, name, email, body }) => fields(squel.update().table(table),{ postid, name, email, body });
exports.save = ({id, postid, name, email, body }) => (id)?this.update({ postid, name, email, body }).where('id = ?',id):this.insert({ postid, name, email, body });

exports.getByPk = id => this.get().where('id = ?', id);
exports.removeByPk = id => this.remove().where("id = ?", id);
exports.updateByPk = ( id, model ) => this.update(model).where("id = ?", id);

exports.expand = async ( models, fields, client ) => {
    if(fields.includes("post")){
        var uids =_.uniq(_.map(models,"postid"));
        var { rows } = await client.query(Post.get().where("id IN ?",uids).toParam());
        _.each(models, x => x.post=_.find(rows,{id:x.postid}))
    }
}

exports.validate = ({ postid, name, email, body }) => validate({ postid, name, email, body },this.schema);

exports.schema = {
    postid:{
        presence:true,
        numericality: {
            onlyInteger: true,
        }
    },
    name:{
        presence:true,
        length:{
            minimum:10,
            maximum:64
        }
    },
    email:{
        presence:true,
        length:{
            minimum:10,
            maximum:500
        }
    },
    body:{
        presence:true,
        length:{
            minimum:100,
            maximum:500
        }
    }
}