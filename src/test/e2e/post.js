const { apiClient } = require('./config');
const { assert } = require('chai');
const resource = '/posts'

describe('post API', () => {    
    describe('Creación',() => {
        var userid;
        it('Debe crear', async () => {
            try {
                rand = Math.floor((Math.random() * 10000000000) + 1);
                var data = {
                    "name": "ruben"+rand,
                    "username": "rubentest"+rand,
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                const response = await apiClient
                    .post('/users')
                    .send(data)
                    .expect(201);
                const body = response.body;
                userid = body.id;
            } catch (err) {
                throw(err);
            }
        })

        const props = ['id','userid','title','body'];

        it('Debe rechazar sin datos', async() => {
            try {
                await apiClient
                    .post(resource)
                    .send({})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar con datos invalidos', async() => {
            try {
                await apiClient
                    .post(resource)
                    .send({
                        userid:userid,
                        title:"Hola",
                        body:"este es un bodnyldsnlaksngañsgnañslkgnasñlgnasñlASFÑLKADSGÑKDSJGÑAKNGAÑLSKFGNÑLASFDKNGÑSDFLKNGDFÑLKNDSFÑL<ÑLNDFLSDKNFÑSDLKNFÑASDLNngasñngasñngañskngsñ"
                    })
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe crear', async () => {
            try {
                var { body } = await apiClient
                    .post(resource)
                    .send({
                        "userid":userid,
                        "title":"HolaASKFASKFMASFLNASFN",
                        "body":"este es un bodnyldsnlaksngañsgnañslkgnasñlgnasñlASFÑLKADSGÑKDSJGÑAKNGAÑLSKFGNÑLASFDKNGÑSDFLKNGDFÑLKNDSFÑL<ÑLNDFLSDKNFÑSDLKNFÑASDLNngasñngasñngañskngsñ"
                    })
                    .expect(201);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
            } catch (err) {
                throw(err);
            }
        })
    })
    
    const props = ['id','userid','title','body'];
    var postid;
    describe('Editar', () => {        var userid;
        it('Debe crear usuario', async () => {
            try {
                rand = Math.floor((Math.random() * 10000000000) + 1);
                var data = {
                    "name": "ruben"+rand,
                    "username": "rubentest"+rand,
                    "email": "ruben@ruben.cl",
                    "phone": "9123345132453",
                    "website": "https://ruben.cl",
                    "address": {
                        "street": "Kulas Light",
                        "suite": "Apt. 556 asdasd",
                        "city": "Gwenborough",
                        "zipcode": "92998-3874",
                        "lat": "-37.3159",
                        "lng": "81.1496"
                    },
                    "company": {
                        "name": "Romaguera-Crona",
                        "catchphrase": "Multi-layered client-server neural-net",
                        "bs": "harness real-time e-markets"
                    }
                };
                const response = await apiClient
                    .post('/users')
                    .send(data)
                    .expect(201);
                const body = response.body;
                userid = body.id;
            } catch (err) {
                throw(err);
            }
        })

        it('Debe crear post', async () => {
            try {
                var { body } = await apiClient
                    .post(resource)
                    .send({
                        "userid":userid,
                        "title":"HolaASKFASKFMASFLNASFN",
                        "body":"este es un bodnyldsnlaksngañsgnañslkgnasñlgnasñlASFÑLKADSGÑKDSJGÑAKNGAÑLSKFGNÑLASFDKNGÑSDFLKNGDFÑLKNDSFÑL<ÑLNDFLSDKNFÑSDLKNFÑASDLNngasñngasñngañskngsñ"
                    })
                    .expect(201);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
                    postid = body.id;
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar si el post no existe', async() => {
            try {
                await apiClient
                    .put(resource+"/10000000")
                    .send({title:"test"})
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar editar sin datos', async() => {
            try {
                await apiClient
                    .put(resource+"/"+postid)
                    .send({})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe rechazar editar con datos invalidos', async() => {
            try {
                await apiClient
                    .put(resource+"/"+postid)
                    .send({title:"invalido"})
                    .expect(422);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe editar', async() => {
            try {
                await apiClient
                    .put(resource+"/"+postid)
                    .send({title:"Este tutlo es valido porque si"})
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
    })
    describe('Obtener', () => {
        it('Debe obtener todos los posts', async() => {
            try {
                var { body } = await apiClient
                    .get(resource)
                    .expect(200);
                    assert.isObject(body[0]);
                    assert.hasAllKeys(body[0], props);
            } catch (err) {
                throw(err);
            }
        })
        it('No debe rechazar post que no existe', async() => {
            try {
                await apiClient
                    .get(resource+"/"+(postid+100))
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe obtener post por ID', async() => {
            try {
                var { body } = await apiClient
                    .get(resource+"/"+postid)
                    .expect(200);
                    assert.isObject(body);
                    assert.hasAllKeys(body, props);
            } catch (err) {
                throw(err);
            }
        })
    })
    describe('Eliminación', () => {
        it('Debe rechazar eliminar si el post no existe', async() => {
            try {
                await apiClient
                    .delete(resource+`/${postid+100}`)
                    .expect(404);
            } catch (err) {
                throw(err);
            }
        })
        it('Debe eliminar por ID', async() => {
            try {
                await apiClient
                    .delete(resource+`/${postid}`)
                    .expect(200);
            } catch (err) {
                throw(err);
            }
        })
    })
})