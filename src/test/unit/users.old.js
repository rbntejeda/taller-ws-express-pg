const { assert } = require("chai");
const usersServices = require("../../services/usersServices");

describe('Repositorio Users', () => {
    let userId;
    const props = ['id','name','username','email','phone','website','company','address'];
    it('Debe guardar un usuario', async () => {
        const data = {
            name:"ruben",
            username:"rubentest",
            email:"ruben@ruben.cl",
            phone:"9123345",
            website:"ruben.cl",
            address: {
                "street": "Kulas Light",
                "suite": "Apt. 556",
                "city": "Gwenborough",
                "zipcode": "92998-3874",
                "geo": {
                    "lat": "-37.3159",
                    "lng": "81.1496"
                }
            },              
            company: {
                "name": "Romaguera-Crona",
                "catchPhrase": "Multi-layered client-server neural-net",
                "bs": "harness real-time e-markets"
            }
        }
        try {
            const user = await usersServices.insert(data);
            assert.isObject(user);
            assert.hasAllKeys(user, props);
            userId = user.id;
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    });
    it('Debe obtener un usuario por ID', async () => {
        try {
            const user = await usersServices.getByPK(userId);
            assert.isObject(user);
            assert.hasAllKeys(user, props);
            userId = user.id;
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    });
    it('Debe obtener todos los usuarios', async () => {
        try {
            const user = await usersServices.getAll();
            assert.isArray(user);
            assert.isNotEmpty(user);
            assert.hasAllKeys(user[0],props);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    });
    it('Debe actualizar un usuario', async () => {
        try {
            const user = await usersServices.update({
                id:userId,
                name:"ruben_test2",
                username:"rubentest",
                email:"ruben@ruben.cl",
                phone:"9123345",
                website:'ruben.cl'
            })
            assert.isObject(user);
            assert.hasAllKeys(user, props);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    })
    it('Debe eliminar un usuario', async () => {
        try {
            const user = await usersServices.remove(userId);
            if(user.rowCount!=1)
            // assert.hasAllKeys(user, props);
            return Promise.resolve();
        } catch (error) {
            return Promise.reject(error);
        }
    })
});