var { assert } = require("chai");

var { User, Album, Photo } = require("../../services");

var db = require("../../db");

describe('Photo', function() {
    let userId;
    const props = ['id','albumid','title','url','thumbnailurl'];
    let photoid;
    it('Debe guardar un Photo', async () => {
        try {
            var client = await db.connect();
            //Agrega información previa
            var query = User.insert({
                name:"ruben_test2",
                username:"rubentest",
                email:"ruben@ruben.cl",
                phone:"9123345",
                website:"ruben.cl",
            });
            var { rows } = await client.query(query.toParam());
            userid = rows[0].id;
            
            //agrega un Album
            var query = Album.insert({
                userid:userid,
                title:"Titulo test"
            });
            var { rows } = await client.query(query.toParam());            
            albumid = rows[0].id;

            var query = Photo.insert({
                albumid:albumid,
                title:"Titulo test",
                url:"https:\\hodlasdmas.cl",
                thumbnailurl:"https:\\hodlasdmas.cl",
            });
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
            photoid = rows[0].id;
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener el Photo por ID', async() => {
        try {
            var client = await db.connect();
            var query = Photo.getByPk(photoid);
            var { rows } = await client.query(query.toParam());
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe obtener todos los Photos', async() => {
        try {
            var client = await db.connect();
            var query = Photo.get();
            var { rows } = await client.query(query.toParam());
            assert.isNotEmpty(rows);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe actualizar el Photo por ID', async() => {
        try {
            var client = await db.connect();
            var query = Photo.updateByPk(photoid,{title:"test_update"});
            var { rows } = await client.query(query.toParam());
            assert.equal(rows[0].title,"test_update");
            assert.isObject(rows[0]);
            assert.hasAllKeys(rows[0], props);
        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
    it('Debe eliminar el Photo Photo por ID', async() => {
        try {
            var client = await db.connect();
            var query = Photo.removeByPk(photoid);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.isEmpty(rows);
            assert.equal(rowCount,1);
            // Eliminar usuario de prueba
            var query = User.removeByPk(userid);
            var { rows, rowCount } = await client.query(query.toParam());
            assert.equal(rowCount,1);

        } catch (err) {
            throw(err);
        } finally {
            client.release();
        }
    });
  });